#include <iostream>
#include <queue>

using namespace std;

struct tree_node {
    char c;
    bool colour, visited;
    tree_node *left_node = nullptr, *right_node = nullptr, *parent = nullptr;

    explicit tree_node(char c) {
        this->c=c;
        colour = true;
        visited = false;
    }
    ~tree_node() {
        delete left_node;
        delete right_node;
    }
};

class RedBlackTree{
    private:
        tree_node *root_node=nullptr;
    public:
        RedBlackTree() = default;

        void InsertElement(char key) {

            tree_node *new_node = nullptr;
            tree_node *current_node = nullptr;

            if (root_node == nullptr) {
                root_node = new tree_node(key);
            } else {
                deque<tree_node*> q;
                q.push_back(root_node);

                while (!q.empty()) {
                    tree_node *temp = q.front();
                    q.pop_back();

                    if (key > temp->c) {
                        if (temp->right_node != nullptr) q.push_back(temp->right_node);
                        else temp->right_node = new tree_node(key);
                        temp->right_node->parent = temp;
                        current_node = temp->right_node;
                    } else {
                        if (temp->left_node != nullptr) q.push_back(temp->left_node);
                        else temp->left_node = new tree_node(key);
                        temp->left_node->parent = temp;
                        current_node = temp->left_node;
                    }
                }
                new_node = current_node;
                if (new_node->parent != nullptr && new_node->parent->parent != nullptr) {
                    while (new_node->parent->colour) {
                        if (new_node->parent->parent->left_node == new_node->parent) {
                            if (new_node->parent->parent->right_node != nullptr
                            && new_node->parent->parent->right_node->colour) {
                                new_node->parent->colour = false;
                                new_node->parent->parent->right_node->colour = false;
                                new_node->parent->parent->colour = true;
                                current_node = new_node->parent->parent;
                            } else {
                                if (new_node->parent->right_node == new_node) {
                                    tree_node *temp = new_node->parent;
                                    tree_node *temp2 = new_node->left_node;
                                    new_node->parent = new_node->parent->parent;
                                    new_node->left_node = temp;
                                    temp->parent = new_node;
                                    temp->right_node = temp2;
                                    new_node->parent->left_node = new_node;
                                    if (temp2 != nullptr) temp2->parent = temp;
                                    current_node = new_node->left_node;
                                }
                                new_node = current_node;
                                tree_node *temp = new_node->parent->parent;
                                tree_node *temp2 = new_node->parent->right_node;
                                bool flag1 = false, flag2 = false;
                                if (new_node->parent->parent->parent != nullptr
                                && new_node->parent->parent->parent->right_node == new_node->parent->parent) flag1 = true;
                                else if (new_node->parent->parent->parent != nullptr
                                && new_node->parent->parent->parent->left_node == new_node->parent->parent) flag2 = true;
                                new_node->parent->parent = new_node->parent->parent->parent;
                                new_node->parent->right_node = temp;
                                new_node->parent->right_node->parent = new_node->parent;
                                new_node->parent->colour = false;
                                new_node->parent->right_node->colour = true;
                                new_node->parent->right_node->left_node = temp2;
                                if (new_node->parent->right_node->left_node != nullptr)
                                    new_node->parent->right_node->left_node->parent = new_node->parent->right_node;
                                if (flag1) new_node->parent->parent->right_node = new_node->parent;
                                else if (flag2) new_node->parent->parent->left_node = new_node->parent;
                                else {
                                    root_node = new_node->parent;
                                    return;
                                }
                            }
                        } else if (new_node->parent->parent->right_node == new_node->parent) {
                            if (new_node->parent->parent->left_node != nullptr
                            && new_node->parent->parent->left_node->colour) {
                                new_node->parent->colour = false;
                                new_node->parent->parent->left_node->colour = false;
                                new_node->parent->parent->colour = true;
                                current_node = new_node->parent->parent;
                            } else {
                                if (new_node->parent->left_node == new_node) {
                                    tree_node *temp = new_node->parent;
                                    tree_node *temp2 = new_node->right_node;
                                    new_node->parent = new_node->parent->parent;
                                    new_node->right_node = temp;
                                    temp->parent = new_node;
                                    temp->left_node = temp2;
                                    new_node->parent->right_node = new_node;
                                    if (temp2 != nullptr) temp2->parent = temp;
                                    current_node = new_node->right_node;
                                }
                                new_node = current_node;
                                tree_node *temp = new_node->parent->parent;
                                tree_node *temp2 = new_node->parent->left_node;
                                bool flag1 = false, flag2 = false;
                                if (new_node->parent->parent->parent != nullptr
                                    && new_node->parent->parent->parent->right_node == new_node->parent->parent) flag1 = true;
                                else if (new_node->parent->parent->parent != nullptr
                                         && new_node->parent->parent->parent->left_node == new_node->parent->parent) flag2 = true;
                                new_node->parent->parent = new_node->parent->parent->parent;
                                new_node->parent->left_node = temp;
                                new_node->parent->left_node->parent = new_node->parent;
                                new_node->parent->colour = false;
                                new_node->parent->left_node->colour = true;
                                new_node->parent->left_node->right_node = temp2;
                                if (flag1) new_node->parent->parent->right_node = new_node->parent;
                                else if (flag2) new_node->parent->parent->left_node = new_node->parent;
                                else {
                                    root_node = new_node->parent;
                                    return;
                                }
                            }
                        }
                        if (current_node->parent != nullptr) new_node = current_node;
                    }
                }
            }
            root_node->colour = false;
        }

        pair<char*,char*> getChildrenNodesValues(char *key=NULL) {
            if (key == nullptr) {
                return pair<char*, char*> (&root_node->left_node->c, &root_node->right_node->c);
            }

            deque<tree_node*> q;
            q.push_back(root_node);

            while (!q.empty()) {
                tree_node *temp = q.front();
                q.pop_back();

                if (*key == temp->c) return pair<char*, char*> (&temp->left_node->c, &temp->right_node->c);

                if (*key > temp->c) {
                    if (temp->right_node != nullptr) q.push_back(temp->right_node);
                } else {
                    if (temp->left_node != nullptr) q.push_back(temp->left_node);
                }
            }
            return pair<char *, char *>(nullptr, nullptr);
        }

        bool isRedNode(char *key=nullptr) {
            if (root_node->c == *key) return root_node->colour;

            deque<tree_node*> q;
            q.push_back(root_node);

            while (!q.empty()) {
                tree_node *temp = q.front();
                q.pop_back();

                if (*key == temp->c) return temp->colour;

                if (*key > temp->c) {
                    if (temp->right_node != nullptr) q.push_back(temp->right_node);
                } else {
                    if (temp->left_node != nullptr) q.push_back(temp->left_node);
                }
            }
            return false;
        }

        char *getRootNode() {
            return &root_node->c;
        }

        string PreOrderTraversal() {
            deque<tree_node*> qpre;
            qpre.push_back(root_node);
            string str;
            while (!qpre.empty()) {
                tree_node *temp = qpre.front();
                qpre.pop_front();
                str.push_back(temp->c);
                if (temp->right_node != nullptr) qpre.push_front(temp->right_node);
                if (temp->left_node != nullptr) qpre.push_front(temp->left_node);
            }
            return str;
        }

        string PostOrderTraversal() {
            deque<tree_node*> qpost;
            qpost.push_back(root_node);
            string str;
            while (!qpost.empty()) {
                tree_node *temp = qpost.front();
                if (temp->right_node != nullptr && !temp->visited) qpost.push_front(temp->right_node);
                if (temp->left_node != nullptr && !temp->visited) qpost.push_front(temp->left_node);
                if (temp->left_node == nullptr && temp->right_node == nullptr || temp->visited) {
                    str.push_back(temp->c);
                    qpost.pop_front();
                    temp->visited = false;
                } else temp->visited = true;
            }
            return str;
        }
};

int main () {
    RedBlackTree tree = RedBlackTree();
    tree.InsertElement('x');
    tree.InsertElement('b');
    tree.InsertElement('c');
    tree.InsertElement('d');
    tree.InsertElement('e');
    tree.InsertElement('f');
    tree.InsertElement('g');
    tree.InsertElement('h');
    tree.InsertElement('i');
    tree.InsertElement('j');
    tree.InsertElement('k');
    tree.InsertElement('s');
    tree.InsertElement('m');
    tree.InsertElement('n');
    tree.InsertElement('o');
    tree.InsertElement('p');
    tree.InsertElement('q');
    tree.InsertElement('r');
    tree.InsertElement('l');
    tree.InsertElement('t');
    tree.InsertElement('u');
    tree.InsertElement('v');
    tree.InsertElement('a');
    tree.InsertElement('y');
    tree.InsertElement('z');
    cout << tree.isRedNode("g") << tree.isRedNode("l") << tree.isRedNode("d")
    << tree.isRedNode("h") << tree.isRedNode("i") << tree.isRedNode("m") << tree.isRedNode("o")
    << tree.isRedNode("p") << "\n";


    cout << tree.PreOrderTraversal() << "\n";
    cout << tree.PostOrderTraversal() << "\n";
    char key='v';
    pair<char*,char*> ch = tree.getChildrenNodesValues();
    cout << (ch.first ? ch.first:"NULL") << " " << (ch.second ? ch.second:"NULL") << endl;
    key='i';
    ch = tree.getChildrenNodesValues(&key);
    cout << (ch.first ? ch.first:"NULL") << " " << (ch.second ? ch.second:"NULL") << endl;
}