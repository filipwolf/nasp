#include <iostream>
#include <algorithm>
#include <queue>

using namespace std;

uint solve(const uint N, const uint M, uint16_t *const problem, vector<pair<uint, uint>> &solution) {

    uint *sum = new uint[M * N];
    uint indexI = N - 1;
    uint indexJ = M - 1;

    for (int i = 0; i < M; i++) {
        for (int j = 0; j < N; j++) {
            if (i == 0 && j == 0) sum[0] = problem[0];
            else if (i == 0) sum[j * M + i] = problem[j * M + i] + sum[j * M + i - M];
            else if (j == 0) sum[j * M + i] = problem[j * M + i] + sum[j * M + i - 1];
            else sum[j * M + i] = problem[j * M + i] + min(sum[j * M + i - 1], sum[j * M + i - M]);
        }
    }

    solution.emplace_back(indexI, indexJ);

    while(!(indexI == 0 && indexJ == 0)) {
        if (indexJ == 0) indexI -= 1;
        else if (indexI == 0) indexJ -= 1;
        else sum[indexI * M + indexJ - M] < sum[indexI * M + indexJ - 1] ? indexI -= 1 : indexJ -= 1;
        solution.emplace_back(indexI, indexJ);
    }

    reverse(solution.begin(), solution.end());
    uint retN = sum[N * M - 1];
    delete [] sum;

    return retN;
}

int main() {
    vector<pair<uint, uint>> b;
    uint16_t a[] = {100, 200, 1000, 0, 200, 100, 600, 0, 300, 1600, 100, 0};
    cout << solve(3, 4, a, b) << "\n";
    for (auto & i : b) cout << i.first << "," << i.second << "\n";
    return 0;
}
