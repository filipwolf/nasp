#include <iostream>
#include "vector"
#include "set"

struct Edge {
    char from;
    char to;
    int weight;
    Edge(char f, char t, int w) : from{f}, to{t}, weight{w} {}
    Edge() = default;
};

using namespace std;
using edges_t = vector<Edge>;
using vertices_t = set<char>;
using graph_t = pair<vertices_t, edges_t>;

bool CycleDetectionDFS(pair<char, int> v, pair<char, int> u, vector<pair<char, int>> &vertices, edges_t edges, edges_t &cycle) {
    v.second++;
    for (auto & vertex : vertices) {
        if (vertex.first == v.first) {
            vertex = v;
            break;
        }
    }
    for (int i = 0; i < vertices.size(); ++i) {
        if (vertices[i].first != v.first && vertices[i].first != u.first && vertices[i].first != '0') {
            for (int j = 0; j < edges.size(); ++j) {
                if (vertices[i].first == edges[j].to && v.first == edges[j].from ||
                    vertices[i].first == edges[j].from && v.first == edges[j].to) {
                    cycle.push_back(edges[j]);
                    if (vertices[i].second == 0) {
                        if(CycleDetectionDFS(make_pair(vertices[i].first, vertices[i].second), v, vertices, edges, cycle)) return true;
                        else cycle.pop_back();
                    } else return true;
                }
            }
        }
    }
    return false;
}

graph_t dijkstra(graph_t &graph) {
    edges_t returnEdges;
    vertices_t returnVertices;
    edges_t edges = graph.second;
    int edgesSize = graph.second.size();
    vector<pair<char, int>> vertices;

    for (int i = 0; i < edgesSize; ++i) {
        returnEdges.push_back(edges[i]);
        bool flag1 = true;
        bool flag2 = true;
        for (auto & vertice : vertices) {
            if (vertice.first == edges[i].from) flag1 = false;
            if (vertice.first == edges[i].to) flag2 = false;
        }
        if (flag1) vertices.emplace_back(edges[i].from, 0);
        if (flag2) vertices.emplace_back(edges[i].to, 0);

        vector<pair<char, int>> vertices2 = vertices;
        edges_t cycle;
        if (CycleDetectionDFS(vertices2[0], make_pair('0', 0), vertices2, returnEdges, cycle)) {
            int maxWeight = 0;
            Edge remove{};
            vector<char> appeared;
            for (unsigned int j = cycle.size() - 1; j >= 0; --j) {
                bool flag3 = true;
                bool flag4 = true;
                for (char k : appeared) {
                    if (cycle[j].to == k) flag3 = false;
                    if (cycle[j].from == k) flag4 = false;
                }
                if (flag3) appeared.push_back(cycle[j].to);
                if (flag4) appeared.push_back(cycle[j].from);
                if (cycle[j].weight > maxWeight) {
                    maxWeight = cycle[j].weight;
                    remove = cycle[j];
                }
                if (!flag3 && !flag4) break;
            }
            for (int j = 0; j < returnEdges.size(); ++j) {
                if (returnEdges[j].from == remove.from && returnEdges[j].to == remove.to) {
                    returnEdges.erase(returnEdges.begin()+j);
                    break;
                }
            }
            for (int j = 0; j < vertices.size(); ++j) {
                bool flag = true;
                for (auto & returnEdge : returnEdges) {
                    if (vertices[j].first == returnEdge.from || vertices[j].first == returnEdge.to) flag = false;
                }
                if (flag) {
                    vertices.erase(vertices.begin()+j);
                    break;
                }
            }
        }
    }
    for (auto & vertex : vertices) {
        returnVertices.insert(vertex.first);
    }
    for (auto & returnEdge : returnEdges) {
        cout << returnEdge.from << "--" << returnEdge.to << "\n";
    }
    for (auto & vertice : vertices) {
        cout << vertice.first << "\n";
    }
    return make_pair(returnVertices, returnEdges);
}

int main() {
    vertices_t vertices;
    vertices.insert('A');   vertices.insert('B');
    vertices.insert('C');   vertices.insert('D');
    vertices.insert('E');   vertices.insert('F');
    vertices.insert('G');   vertices.insert('H');
    vertices.insert('I');   vertices.insert('J');
    vertices.insert('K');   vertices.insert('L');
    vertices.insert('M');   vertices.insert('N');
    vertices.insert('O');   vertices.insert('P');
    vertices.insert('R');   vertices.insert('S');
    vertices.insert('T');   vertices.insert('U');
    vertices.insert('V');   vertices.insert('W');
    vertices.insert('X');   vertices.insert('Y');
    edges_t edges;
    edges.push_back(*(new Edge('A', 'B', 2)));
    edges.push_back(*(new Edge('A', 'C', 4)));
    edges.push_back(*(new Edge('B', 'C', 5)));
    edges.push_back(*(new Edge('B', 'D', 2)));
    edges.push_back(*(new Edge('B', 'E', 7)));
    edges.push_back(*(new Edge('C', 'D', 1)));
    edges.push_back(*(new Edge('C', 'H', 3)));
    edges.push_back(*(new Edge('C', 'G', 8)));
    edges.push_back(*(new Edge('D', 'H', 2)));
    edges.push_back(*(new Edge('D', 'G', 7)));
    edges.push_back(*(new Edge('E', 'F', 8)));
    edges.push_back(*(new Edge('E', 'H', 1)));
    edges.push_back(*(new Edge('F', 'J', 2)));
    edges.push_back(*(new Edge('F', 'I', 4)));
    edges.push_back(*(new Edge('F', 'N', 1)));
    edges.push_back(*(new Edge('G', 'H', 4)));
    edges.push_back(*(new Edge('G', 'L', 3)));
    edges.push_back(*(new Edge('G', 'K', 7)));
    edges.push_back(*(new Edge('H', 'I', 3)));
    edges.push_back(*(new Edge('H', 'M', 2)));
    edges.push_back(*(new Edge('H', 'L', 7)));
    edges.push_back(*(new Edge('I', 'N', 6)));
    edges.push_back(*(new Edge('I', 'M', 2)));
    edges.push_back(*(new Edge('J', 'O', 5)));
    edges.push_back(*(new Edge('K', 'P', 14)));
    edges.push_back(*(new Edge('L', 'T', 8)));
    edges.push_back(*(new Edge('L', 'P', 7)));
    edges.push_back(*(new Edge('M', 'N', 9)));
    edges.push_back(*(new Edge('M', 'S', 4)));
    edges.push_back(*(new Edge('M', 'R', 2)));
    edges.push_back(*(new Edge('N', 'O', 9)));
    edges.push_back(*(new Edge('N', 'V', 10)));
    edges.push_back(*(new Edge('N', 'R', 1)));
    edges.push_back(*(new Edge('O', 'V', 3)));
    edges.push_back(*(new Edge('R', 'U', 6)));
    edges.push_back(*(new Edge('R', 'T', 4)));
    edges.push_back(*(new Edge('S', 'V', 2)));
    edges.push_back(*(new Edge('U', 'V', 11)));
    edges.push_back(*(new Edge('U', 'X', 5)));
    edges.push_back(*(new Edge('V', 'W', 4)));
    edges.push_back(*(new Edge('V', 'Y', 8)));
    edges.push_back(*(new Edge('V', 'X', 6)));
    edges.push_back(*(new Edge('W', 'Y', 9)));

    graph_t data = make_pair(vertices, edges);
    graph_t graph = dijkstra(data);
    return 0;
}
